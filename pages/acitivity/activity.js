// pages/acitivity/activity.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    focus: false,
    inputValue: '',
    use: [{
      "use_name": "全部"
    },
    {
      "use_name": "经济实惠型"
    },
    {
      "use_name": "家用学习型"
    },
    {
      "use_name": "豪华发烧型"
    },
    {
      "use_name": "疯狂游戏型"
    },
    {
      "use_name": "商务办公型"
    },
    {
      "use_name": "经济实惠型"
    },
    {
      "use_name": "家用学习型"
    },
    ],
    state: '',
  },
  bindButtonTap: function () {
    this.setData({
      focus: true
    })
  },

  select_use: function (e) {
    this.setData({
      state: e.currentTarget.dataset.key,
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})