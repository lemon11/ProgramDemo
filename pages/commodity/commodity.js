// pages/commodity/commodity.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    maskFlag: true,
    select_item: 0,
    select: [
      false,
      false,
      false,
    ],
    grades: [
      [ 
        '猛犸机器人1班','猛犸机器人2班','口才1班' 
      ],
      [
        '猛犸机器人3班', '猛犸机器人4班', '口才1班'
      ],
      [ 
        '猛犸机器人5班', '猛犸机器人6班', '口才1班'
      ],
    ],
    top_name: [
      '区域',
      '品类',
      '活动优先',
    ],
    imgUrl: [
      "/images/commodity_down.png",
      "/images/commodity_down.png",
      "/images/commodity_down.png",
    ],
    dataList: [
      {
        goods_id: 1,
        goods_title: '商品标题1',
        goods_img: 'http://static.home.mi.com/app/shop/img?id=shop_48ebe9e693ade1766877e0f8adf425f7.png&w=420&h=240&crop=a_90_0_240_240',
        goods_xiaoliang: '0',
        goods_price: '60'
      }, {
        goods_id: 1,
        goods_title: '商品标题2',
        goods_img: 'http://static.home.mi.com/app/shop/img?id=shop_c2cf209c66a22818c7f5c269f6bbff12.jpeg&w=420&h=240&crop=a_90_0_240_240',
        goods_xiaoliang: '0',
        goods_price: '70'
      }, {
        goods_id: 1,
        goods_title: '商品标题3',
        goods_img: 'http://static.home.mi.com/app/shop/img?id=shop_8dec2f08e5dd9d08b440f77a36e39e16.png&w=420&h=240&crop=a_90_0_240_240',
        goods_xiaoliang: '0',
        goods_price: '80'
      }, {
        goods_id: 1,
        goods_title: '商品标题4',
        goods_img: 'http://static.home.mi.com/app/shop/img?id=shop_35a026ff12d476496f91d121911af0ce.jpg&crop=a_90_0_240_240',
        goods_xiaoliang: '0',
        goods_price: '90'
      }, {
        goods_id: 1,
        goods_title: '商品标题5',
        goods_img: 'http://static.home.mi.com/app/shop/img?id=shop_0b23f4b364ee73bc86b280cc7397638c.jpg&w=420&h=240&crop=a_90_0_240_240',
        goods_xiaoliang: '0',
        goods_price: '110'
      }
    ],
  },

  bindShowMsg(e) {
    
    var that = this;
    var a = e.currentTarget.dataset.tapindex
    that.data.select[a] = !that.data.select[a],
    that.data.maskFlag = !that.data.maskFlag,
    this.setData({ 
      select: that.data.select,
      select_item : a,
      maskFlag: that.data.maskFlag,
      }, () => {
        if (that.data.select[a]) {
          that.data.imgUrl[a] = "/images/commodity_up.png",
          this.setData({
            imgUrl:that.data.imgUrl,
          })  
    } else {
        that.data.imgUrl[a] = "/images/commodity_down.png",
        this.setData({
            imgUrl: that.data.imgUrl,
        })
     }
    })
  },

  mySelect(e) {
    
    var that = this
    that.data.select[this.data.select_item] = false
    that.data.imgUrl[this.data.select_item] = "/images/commodity_down.png"
    var name = e.currentTarget.dataset.name
    var index = e.currentTarget.dataset.indexk
    that.data.top_name[that.data.select_item] = name

    this.setData({
      top_name: that.data.top_name,
      select: that.data.select,
      imgUrl: that.data.imgUrl,
      maskFlag: true,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})